from django.db import models
from django.conf import settings
from django.db.models.aggregates import Sum

from uuid import uuid4


def movie_directory_path_with_uuid(instance, filename):
    return '{}/{}'.format(instance.movie_id, uuid4())


class MovieImage(models.Model):
    # ImageField uses Pillow
    image = models.ImageField(upload_to=movie_directory_path_with_uuid)
    uploaded = models.DateTimeField(auto_now_add=True)
    movie = models.ForeignKey('Movie', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)


class PersonManager(models.Manager):
    def all_with_prefetch_movies(self):
        qs = self.get_queryset()                        ## lazy method
        ## query in once
        return qs.prefetch_related('directed',          ## related name from model Movie to get
                                   'writing_credits',   ## relevant Movie which has this person
                                   'role_set__movie')   ## model Role field movie


class Person(models.Model):
    first_name = models.CharField(max_length=140)
    last_name = models.CharField(max_length=140)
    born = models.DateField()
    died = models.DateField(null=True, blank=True)

    objects = PersonManager()       ## for prefetch_related

    class Meta:
        ordering = ('last_name', 'first_name')

    def __str__(self):
        if self.died:
            return '{}, {} ({}-{})'.format(
                self.last_name,
                self.first_name,
                self.born,
                self.died)
        return '{}, {} ({})'.format(
                self.last_name,
                self.first_name,
                self.born)


class MovieManager(models.Manager):
    def all_with_related_persons(self):
        qs = self.get_queryset()
        qs = qs.select_related('director')              # used when 1 to 1, e.g. FK
        qs = qs.prefetch_related('writers', 'actors')   # used when m to m
        return qs                                       # its own fields, not related_name

    def all_with_related_persons_and_score(self):
        qs = self.all_with_related_persons()
        qs = qs.annotate(score=Sum('vote__value'))      # queryset annotate, model__field
                                                        # -> add query expression, e.g. sum
        return qs

    def top_movies(self, limit=10):
        qs = self.get_queryset()
        qs = qs.annotate(vote_sum=Sum('vote__value'))
        qs = qs.exclude(vote_sum=None)
        qs = qs.order_by('-vote_sum')
        qs = qs[:limit]
        return qs


class Movie(models.Model):
    NOT_RATED = 0
    RATED_G = 1
    RATED_PG = 2
    RATED_R = 3
    RATINGS = {
        (NOT_RATED, 'NR - Not rated'),
        (RATED_G, 'G - General audiences'),
        (RATED_PG, 'PG - Parental guidance'),
        (RATED_R, 'R - Restricted'),
    }

    title = models.CharField(max_length=140)
    plot = models.TextField()
    year = models.PositiveIntegerField()

    ## django added method get_xxxx_display(), i.e. get_rating_display()
    ## if used in template, which need not ()
    rating = models.IntegerField(               
        choices=RATINGS,                        
        default=NOT_RATED,
    )
    runtime = models.PositiveIntegerField()
    website = models.URLField(blank=True)       ## varchar(200)
    director = models.ForeignKey(
                to='Person',
                null=True,
                on_delete=models.SET_NULL,      ## db: only set null to this if related person is removed
                related_name='directed',        ## used by model linked to, i.e. Person
                                                ## if not set, default name = movie_set, which
                                                ## confuses with other FK/M2M fields
                blank=True)
    writers = models.ManyToManyField(
                to='Person',
                related_name='writing_credits', ## if not set, default name = movie_set
                blank=True)
    actors = models.ManyToManyField(
                to='Person',
                through='Role',                 ## custom through model
                related_name='acting_credits',
                blank=True)

    objects = MovieManager()

    class Meta:
        ordering = ('-year', 'title')

    def __str__(self):
        return '{} ({})'.format(self.title, self.year)


## must place behind Movie & Person; otherwise error occurs during "makemigrations"
class Role(models.Model):
    '''
    This through model creates 4 RelatedManagers: 
        movie.actors --> related manager of Person
        person.acting_credits --> related manager of Movie
        movie.role_set and person.role_set --> related manager of Role
    '''
    movie = models.ForeignKey(Movie, on_delete=models.DO_NOTHING)
    person = models.ForeignKey(Person, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=140)

    def __str__(self):
        return "{} {} {}".format(self.movie_id, self.person_id, self.name)

    class Meta:
        unique_together = ('movie', 'person', 'name')


class VoteManager(models.Manager):
    def get_vote_or_unsaved_blank_vote(self, movie, user):
        try:
            return Vote.objects.get(movie=movie, user=user)
        except Vote.DoesNotExist:
            # if not found, return a unsaved blank Vote object
            return Vote(movie=movie, user=user)


class Vote(models.Model):
    UP = 1
    DOWN = -1
    VALUE_CHOICES = (
        (UP, "UP",),
        (DOWN, "DOWN",),
    )

    value = models.SmallIntegerField(choices=VALUE_CHOICES)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    voted_on = models.DateTimeField(auto_now=True)

    objects = VoteManager()

    class Meta:
        unique_together = ('user', 'movie')


