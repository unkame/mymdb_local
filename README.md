# App MyMdb (local)
From the book "Building-Django-2.0-Web-Applications" by Tom Aratyn

## Description
For reference. Sample codes of app "MyMdb" from chapter 1 to 4, which is for local development

- finalized codes at the end of chapter 4, not separated chapter by chapter

- scripts_ref/terminal_cmd.txt describes the terminal commands (e.g. django command) used from chapter 1 - 4
